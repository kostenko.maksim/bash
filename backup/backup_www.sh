#!/bin/bash

# Скрипт делает резервную копию бд и каталога сайта
# и размещает всё это на другом сервере по SFTP
# требуется под пользователем один раз подключиться по SFTP
# для добавления отпечатка открытого ключа
# крон на каждый день в 20:00
# 00 20 * * * bash /backup/task/backup_www.sh


arFolders=("site1" "site2" "site3" "site4")
arDB=("site1_db" "site2_db" "site3_db" "site4_db")

# в цикле делаем выгрузку базы данных и каталога сайта
arCount=0
while [ $arCount -le 3 ]; do

  CURRDATE=$(date +%Y%m%d_%H%M)
  DIR_TMP=/backup/www
  NAME_FILE=${arFolders[$arCount]}_$CURRDATE
  USER_DB='user_MySQL'
  PASS_DB='password_MySQL'
  DB=${arDB[$arCount]}
  DIR_BACKUP='/var/www/html/'${arFolders[$arCount]}
  HOST_FTP='hostname_SFTP'
  PORT_FTP='22'
  DIR_FTP='/backup/www'
  USER_FTP='user_SFTP'
  PASS_FTP='password_SFTP'
  PASS_CRYPT='password_ZIP'

  mysqldump -u $USER_DB -p$PASS_DB $DB >$DIR_TMP/$NAME_FILE.sql
  find $DIR_BACKUP -name ".*" -type f -print | zip -r -P $PASS_CRYPT $DIR_TMP/$NAME_FILE.zip $DIR_TMP/*.sql $DIR_BACKUP/* -@
  rm $DIR_TMP/*.sql

  arCount=$(($arCount + 1))
done

echo "Start send files to SFTP...."
export SSHPASS=$PASS_FTP
sshpass -e sftp -P $PORT_FTP -oBatchMode=no -b - $USER_FTP@$HOST_FTP <<!
   cd $DIR_FTP
   put $DIR_TMP/*.zip
   bye
!

if [ $? -ne 0 ]; then
  echo 'ERROR send SFTP code error: '$?
else
  rm $DIR_TMP/*.zip
fi

echo "Finish send files to SFTP...."
echo "Result code: "$?