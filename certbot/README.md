# Certbot

Скрипт создает сертификат для сайта в nginx (обычно используется при первом разворачивании)

Пример запуска команды:

```
# certbot.sh site1.domain.ru
```

# Справка по командам
```shell
certbot run -n --nginx --agree-tos -d site1.domain.ru  -m  admin@shop-polaris.ru  --redirect
certbot run -n --nginx -d site1.domain.ru --redirect
certbot run -n --nginx -d "$sitename" --redirect
```



```
run            Obtain & install a certificate in your current webserver

-n             Run without ever asking for user input. This may
               require additional command line flags; the client will
               try to explain which ones are required if it finds one
               missing (default: False)

--nginx        Obtain and install certificates using Nginx (default:False)


-d             Domain names to apply. For multiple domains you can
               use multiple -d flags or enter a comma separated list
               of domains as a parameter. The first domain provided
               will be the subject CN of the certificate, and all
               domains will be Subject Alternative Names on the
               certificate. The first domain will also be used in
               some software user interfaces and as the file paths
               for the certificate and related material unless
               otherwise specified or you already have a certificate
               with the same name. In the case of a name collision it
               will append a number like 0001 to the file path name.


-m             Email used for registration and recovery contact. Use
               comma to register multiple emails, ex:
               u1@example.com,u2@example.com. (default: Ask).

--redirect     Automatically redirect all HTTP traffic to HTTPS for
               the newly authenticated vhost. (default: Ask)

--agree-tos    Agree to the ACME server's Subscriber Agreement
```

