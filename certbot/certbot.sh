#!/bin/bash

# Скрипт создает сертификат для сайта в nginx (обычно используется при первом разворачивании)

branch=${1//.}
branch=${branch,,}
branch=${branch//_}
branch=${branch//-}
echo branch "$branch";

if [ "$branch" = "" ]
then
	echo "No branch"
else
	sitename="$branch"
	echo -e "Enabling HTTPS & Deploying certificate for $sitename"
	sudo certbot run -n --nginx -d "$sitename" --redirect

fi

